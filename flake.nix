{
  description = "Flake for developing myemma in python 3";
  # Flake inputs
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  # Flake outputs
  outputs = {self, ...} @ inputs: let
    forAllSystems = inputs.nixpkgs.lib.genAttrs [
      "aarch64-linux"
      # "i686-linux"
      "x86_64-linux"
      "aarch64-darwin"
      "x86_64-darwin"
    ];
    treefmtEval = forAllSystems (
      system: let
        pkgs = inputs.nixpkgs.legacyPackages.${system};
      in
        inputs.treefmt-nix.lib.evalModule pkgs {
          # Used to find the project root
          config = {
            projectRootFile = "flake.nix";
            programs = {
              alejandra.enable = true;
              deadnix.enable = true;
              statix.enable = true;
              black.enable = true;
              prettier = {
                enable = true;
                includes = [
                  "*.cjs"
                  "*.css"
                  # "*.html" # Does some strange formatting to html template files, hence using djlint as below instead
                  "*.js"
                  "*.json"
                  "*.json5"
                  "*.jsx"
                  "*.md"
                  "*.mdx"
                  "*.mjs"
                  "*.scss"
                  "*.ts"
                  "*.tsx"
                  "*.vue"
                  "*.yaml"
                  "*.yml"
                ];
              };
            };
            settings = {
              global.excludes = [
              ];
              formatter."djlint" = {
                command = "${pkgs.djlint}/bin/djlint";
                options = ["--reformat"];
                includes = ["*.html"];
              };
            };
          };
        }
    );
  in {
    # Development environment output
    devShells = forAllSystems (
      system: let
        pkgs = inputs.nixpkgs.legacyPackages.${system};
      in {
        default = pkgs.mkShell {
          # The Nix packages provided in the environment
          packages = [
            pkgs.python311
            pkgs.python311Packages.pip
          ];
          shellHook = ''
            python -m venv .venv
            source .venv/bin/activate
            pip install -r requirements.txt
          '';
        };
      }
    );
    formatter = forAllSystems (
      system: let
        pkgs = inputs.nixpkgs.legacyPackages.${system};
      in
        treefmtEval.${pkgs.system}.config.build.wrapper
    );
    # for `nix flake check`
    checks = forAllSystems (
      system: let
        pkgs = inputs.nixpkgs.legacyPackages.${system};
      in {
        formatting = treefmtEval.${pkgs.system}.config.build.check self;
      }
    );
  };
}
